package CyclicBarrierEx;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class WorkerThread implements Runnable {

	private CyclicBarrier barrier = null;
	private int name = 0;
	
	public WorkerThread(CyclicBarrier barrier, int name)
	{
		this.name = name;
		this.barrier = barrier;
	}
	
	public void run()
	{
		 DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		 String s = fmt.format(new Date());
		 System.out.println(s+"=Doing some work for thread: " + this.name);
		 
		 try{
			 Thread.sleep(name * 1000);
			 s = fmt.format(new Date());
			 System.out.println(s+"=Work done for thread: " + this.name);
			 int count = barrier.await();     //below code will execute only after barrier count reaches to max number. This is the diff between CyclicBarrier vs CountDownLatch
			 s = fmt.format(new Date());
			 System.out.println(s+"=Inside thread " + this.name + " count is " + count);
		 }catch(InterruptedException iex)
		 {
			 
		 }catch(BrokenBarrierException ex)
		 {
			 
		 }

	}
	
	

}
