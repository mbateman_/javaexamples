package com.telnet.core.samplePrograms;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

public class F {
	public static class FileVisitor extends SimpleFileVisitor<Path> {
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
				throws IOException {
			System.out.println(file.toRealPath());
			return FileVisitResult.CONTINUE;
		}
	}

	public static void main(String[] args) throws IOException {
		Path startingDir = Paths.get("e:\\temp");
		FileVisitor visitor = new FileVisitor();

		/* List all files from a directory and its subdirectories */
		Files.walkFileTree(startingDir, visitor);

		/* List all the files and directories under a directory */
		Files.walkFileTree(startingDir,
				EnumSet.of(FileVisitOption.FOLLOW_LINKS), 1, visitor);

	}
}
